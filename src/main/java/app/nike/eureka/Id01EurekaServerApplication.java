package app.nike.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Id01EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Id01EurekaServerApplication.class, args);
	}

}
